SHELL=/usr/bin/bash

if [ -f ~/bashfiles/bash-sensible/sensible.bash ]; then
    source ~/bashfiles/bash-sensible/sensible.bash
fi

if [ -f /opt/local/etc/profile.d/bash_completion.sh ]; then
    . /opt/local/etc/profile.d/bash_completion.sh
fi

eval `ssh-agent -s`

# colors for ls, etc.
alias ls="ls -G"
alias ll="ls -G -l -h"
alias ytdlp="yt-dlp -f 'bv+ba/b' -S 'res:1080'"
alias ytdlp-audio="yt-dlp -f m4a --sponsorblock-remove all --postprocessor-args 'ffmpeg:-metadata genre=Podcast'"
alias ytdlp-subs="yt-dlp -f 'bv+ba/b' -S 'res:1080' --write-subs --write-auto-subs --sub-format 'best' --sub-langs 'en,da'"
alias rscp="rsync -aP"
alias rsmv="rsync -aP --remove-source-files"

export LANG="da_DK.UTF-8"
export LC_ALL="da_DK.UTF-8"

compile_prompt() {
	local CONNECTBAR_DOWN=$'\u250C\u2500\u257C'
	local CONNECTBAR_UP=$'\u2514\u2500\u257C'
	local ARROW=$'\u25B6'
	local c_gray='\e[01;30m'
	local c_blue='\e[0;34m'
	local c_cyan='\e[0;36m'
	local c_reset='\e[0m'

	# > Connectbar Down
	# Format:
	#   (newline)(bright colors)(connectbar down)
	PS1="${c_gray}"
	PS1+="$CONNECTBAR_DOWN"

	# > Username
	# Format:
	#   (bracket open)(username)(bracket close)(splitbar)
	PS1+="[${c_blue}\u@\h${c_gray}]"
	PS1+="\n"
	PS1+="$CONNECTBAR_UP"

	# > Working Directory
	# Format:
	#   (bracket open)(working directory)(bracket close)(newline)
	PS1+="[${c_blue}\w${c_gray}]\n"

	# > Arrow
	# NOTE: Color must be escaped with '\[\]' to fix the text overflow bug!
	# Format:
	#   (arrow)(color reset)
	PS1+="$ARROW \[\e[0m\]"

	export PS1
}

compile_prompt
