# source the users bashrc if it exists
if [ -f "${HOME}/.bashrc" ] ; then
    source "${HOME}/.bashrc"
fi

export EDITOR="vim"
export FLEXGET="$HOME/.venvs/flexget/bin"

export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
export PATH="$HOME/bin:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$FLEXGET:$PATH"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Set PATH, MANPATH, etc., for Homebrew.
if [ -f "/opt/homebrew/bin/brew" ] ; then
    eval "$(/opt/homebrew/bin/brew shellenv)"

    # Brew Analytics opt out
    export HOMEBREW_NO_ANALYTICS=1
	# No Brew hints
	export HOMEBREW_NO_ENV_HINTS=1

	source "${HOME}/.work_profile"

fi

test -r ~/.dir_colors && eval $(dircolors ~/.dir_colors)
